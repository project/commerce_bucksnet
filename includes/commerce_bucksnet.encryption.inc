<?php

/**
 * @file
 * Encryption functions for Commerce Bucksnet.
 */

/**
 * Base64Decode an encoded query string.
 *
 * @param string $scrambled
 *   Base64 encoded string.
 *
 * @return string
 *   Base64 decoded string.
 */
function _commerce_bucksnet_base64_decode($scrambled) {
  $corrected = str_replace(" ", "+", $scrambled);
  $output = base64_decode($corrected);
  return $output;
}

/**
 * XOR Encryption.
 *
 * @param string $string
 *   String to be encrypted.
 * @param string $key
 *   Encryption key.
 *
 * @return string
 *   Return the XOT encrypted string.
 */
function _bucksnet_commerce_xor($string, $key) {
  // Initialise key array.
  $keylist = array();
  // Initialise out variable.
  $output = "";

  // Convert $Key into array of ASCII values.
  for ($i = 0; $i < strlen($key); $i++) {
    $keylist[$i] = ord(substr($key, $i, 1));
  }

  // Step through string a character at a time.
  for ($i = 0; $i < strlen($string); $i++) {
    // Get ASCII code from string, get ASCII code from key
    // (loop through with MOD), XOR the two, get the character from the result
    // % is MOD (modulus), ^ is XOR.
    $output .= chr(ord(substr($string, $i, 1)) ^ ($keylist[$i % strlen($key)]));
  }
  return $output;
}

/**
 * Generate a query string for an array of data.
 *
 * @param array $data
 *   Array of data to form the query string.
 *
 * @return string
 *   Returns a formatted URL query string.
 */
function _bucksnet_commerce_generate_query_string(array $data) {
  $keys = array_keys($data);
  $query_string = '';
  foreach ($keys as $key) {
    $query_string .= $key . '=' . $data[$key] . '&';
  }
  $query_string = substr($query_string, 0, strlen($query_string) - 1);
  return $query_string;
}

/**
 * Unpad a string.
 *
 * @param string $data
 *   String of data to pad.
 *
 * @return string
 *   Unpadded string.
 */
function pkcs7unpad($data) {
  $padding = ord($data[strlen($data) - 1]);
  return substr($data, 0, -$padding);
}

/**
 * Pad a string.
 *
 * @param string $plaintext
 *   Original string.
 * @param int $blocksize
 *   Destination block size.
 *
 * @return string
 *   Returns a padding string.
 */
function pkcs7pad($plaintext, $blocksize) {
  $padsize = $blocksize - (strlen($plaintext) % $blocksize);
  return $plaintext . str_repeat(chr($padsize), $padsize);
}

/**
 * Password-Based Key Derivation Function.
 *
 * @param string $algorithm
 *   Algorithm.
 * @param string $password
 *   Password.
 * @param string $salt
 *   Salt.
 * @param int $count
 *   Count.
 * @param int $key_length
 *   Key Length.
 * @param bool $raw_output
 *   Raw.
 *
 * @return string
 *   Derived key.
 */
function pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output = FALSE) {
  $algorithm = strtolower($algorithm);
  if (!in_array($algorithm, hash_algos(), TRUE)) {
    die('PBKDF2 ERROR: Invalid hash algorithm.');
  }
  if ($count <= 0 || $key_length <= 0) {
    die('PBKDF2 ERROR: Invalid parameters.');
  }
  $hash_length = strlen(hash($algorithm, "", TRUE));
  $block_count = ceil($key_length / $hash_length);
  $output = "";
  for ($i = 1; $i <= $block_count; $i++) {

    // $i encoded as 4 bytes, big endian.
    $last = $salt . pack("N", $i);

    // First iteration.
    $last = $xorsum = hash_hmac($algorithm, $last, $password, TRUE);

    // Perform the other $count - 1 iterations.
    for ($j = 1; $j < $count; $j++) {
      $xorsum ^= ($last = hash_hmac($algorithm, $last, $password, TRUE));
    }
    $output .= $xorsum;
  }
  return substr($output, 0, $key_length);
}
